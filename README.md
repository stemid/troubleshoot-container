# Troubleshooting container

Mostly based on the package list from [this container](https://github.com/nicolaka/netshoot).

Runs Debian because it's simpler to use and I don't care about image size for troubleshooting.

Deployed to [my account on Docker Hub](https://hub.docker.com/r/stemid/troubleshoot).

# Deployment to your k8s cluster

## Useful variables

* ``CI_REGISTRY_IMAGE`` - For non-gitlab registries override in CI Variables.
* ``REGISTRY_TAG`` - Tag used, by default set to ``CI_COMMIT_REF_NAME``.
* ``REGISTRY_AUTH_JSON`` - JSON output from buildah login command, or docker login. Used to push image to registry, also used as registrypullcred in k8s deploy.
* ``OVERRIDE_OVERLAY`` - Used to hard code an overlay to use for deployment, if you're trying to deploy a feature branch set this to the overlay you want to use, like master for example.
* ``STAGING_KUBECONFIG`` - Used to deploy to cluster.
* ``K8S_NAMESPACE`` - Namespace to deploy to.
