# stable-slim lacks manpages, among other things.
FROM debian:stable-slim

# Wireshark package asks for input
RUN echo "wireshark-common wireshark-common/install-setuid boolean true" | debconf-set-selections

# Decrease final size of image somewhat by doing this.
RUN printf 'APT::Install-Recommends "false";' > /etc/apt/apt.conf.d/99norecommends && \
  printf 'APT::Install-Suggests "false";' >> /etc/apt/apt.conf.d/99norecommends

RUN DEBIAN_FRONTEND=noninteractive set -ex \
  && apt update \
  && apt install -y \
  apache2-utils \
  awscli \
  bash \
  htop \
  dnsutils \
  knot-dnsutils \
  bird \
  bridge-utils \
  conntrack \
  curl \
  dhcping \
  ethtool \
  file\
  fping \
  httpie \
  iftop \
  iperf \
  iproute2 \
  ipset \
  iptables \
  iptraf-ng \
  iputils-arping \
  iputils-ping \
  iputils-tracepath \
  iputils-clockdiff \
  jq \
  mtr \
  libnet-snmp-perl \
  netcat-openbsd \
  nftables \
  ngrep \
  nmap \
  openssl \
  openssh-client \
  s3cmd \
  scapy \
  socat \
  strace \
  tcpdump \
  tcptraceroute \
  tshark \
  util-linux \
  vim \
  gnupg2 \
  mariadb-client \
  postgresql-client \
  sqlite3 \
  golang \
  less \
  git \
  zstd

ENTRYPOINT /bin/bash
CMD ["-l"]
